import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import java.util.Random;

public class JogoDeAdivinharPalavras {
    private String palavraSecreta = "java";
    private int tentativas = 0;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private boolean isServer;

    private JLabel dicaLabel;
    private JTextField palpiteField;
    private JButton adivinharButton;
    private JTextArea resultadoArea;

    private String[] dicas = {
            "dica: é uma linguagem de programação.",
            "dica: é uma ilha da indonésia.",
            "dica: é uma bebida quente popular."
    };

    private int dicaAtual = 0;
    private PalavraSecreta[] palavrasSecretas = new PalavraSecreta[]{
            new PalavraSecreta("Java", new String[]{
                    "dica: é uma linguagem de programação.",
                    "dica: é uma ilha da indonésia.",
                    "dica: é uma bebida quente popular."
            }),
            new PalavraSecreta("banana", new String[]{
                    "dica: é uma fruta.",
                    "dica: é originárias do sudeste da Ásia.",
                    "dica: é amarela.",
            }),
            new PalavraSecreta("youtube", new String[]{
                    "dica: tem sede em San Bruno, Califórnia.",
                    "dica: sua loga é vermelha.",
                    "dica: é uma plataforma de vídeo.",
            }),
    };

    Random rd = new Random(System.currentTimeMillis());
    int numeroAleatorio = rd.nextInt(palavrasSecretas.length);


    public String getPalavraSecreta() {

        return palavrasSecretas[numeroAleatorio].getPalavra();
    }

    public String getDica() {
        String[] dicas = palavrasSecretas[numeroAleatorio].getDicas();

        return dicas[dicaAtual];
    }


    public JogoDeAdivinharPalavras(boolean isServer) {
        this.isServer = isServer;

        JFrame frame = new JFrame("Jogo de Adivinhar Palavras");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 200);
        frame.setLayout(new GridLayout(3, 1));

        dicaLabel = new JLabel(getDica());
        frame.add(dicaLabel);

        palpiteField = new JTextField();
        frame.add(palpiteField);

        adivinharButton = new JButton("Adivinhar");
        frame.add(adivinharButton);

        resultadoArea = new JTextArea();
        frame.add(resultadoArea);

        adivinharButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tentarAdivinhar();
            }
        });

        frame.setVisible(true);

        if (isServer) {
            iniciarServidor();
        } else {
            conectarAoServidor();
        }
    }

    private void tentarAdivinhar() {
        String palpite = palpiteField.getText();
        tentativas++;

        palavraSecreta = getPalavraSecreta();

        if (palpite.equalsIgnoreCase(palavraSecreta)) {
            resultadoArea.setText("Parabéns! Você acertou a palavra: " + palavraSecreta);
            adivinharButton.setEnabled(false);
        } else if (tentativas >= 3) {
            resultadoArea.setText("Suas tentativas acabaram. A palavra correta era: " + palavraSecreta);
            adivinharButton.setEnabled(false);
        } else {
            resultadoArea.setText("Palpite incorreto. Tente novamente.");
            proximaDica();
        }

        if (clientSocket != null && !clientSocket.isClosed()) {
            enviarResultadoAoCliente();
        }
    }

    private void proximaDica() {
        dicaAtual++;
        String dica = getDica();
        if (dicaAtual < 3) {
            dicaLabel.setText(dica);
        }
    }

    private void iniciarServidor() {
        try {
            serverSocket = new ServerSocket(12345);
            System.out.println("Aguardando um jogador se conectar...");
            clientSocket = serverSocket.accept();
            System.out.println("Jogador conectado!");

            palavraSecreta = getPalavraSecreta();

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

            out.println("Bem-vindo ao jogo de adivinhar palavras!");
            out.println("Dica: É uma linguagem de programação.");

            while (tentativas < 3) {
                out.println("Digite seu palpite: ");
                String palpite = in.readLine();

                if (palpite.equalsIgnoreCase(palavraSecreta)) {
                    out.println("Parabéns! Você acertou a palavra: " + palavraSecreta);
                    break;
                } else {
                    out.println("Palpite incorreto. Tente novamente.");
                    tentativas++;
                }
            }

            in.close();
            out.close();
            clientSocket.close();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void conectarAoServidor() {
        try {
            clientSocket = new Socket("localhost", 12345);

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

            String response;
            while ((response = in.readLine()) != null) {
                resultadoArea.append(response + "\n");
                if (response.contains("Suas tentativas acabaram") || response.contains("Parabéns")) {
                    break;
                }
            }

            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void enviarResultadoAoCliente() {

        palavraSecreta = getPalavraSecreta();

        if (clientSocket != null && !clientSocket.isClosed()) {
            try {
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

                if (tentativas >= 3) {
                    out.println("Suas tentativas acabaram. A palavra correta era: " + palavraSecreta);
                } else {
                    out.println("Palpite incorreto. Tente novamente.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                boolean isServer = args.length > 0 && args[0].equalsIgnoreCase("server");
                new JogoDeAdivinharPalavras(isServer);
            }
        });
    }
}
