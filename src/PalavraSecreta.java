import java.util.Random;

public class PalavraSecreta {
    private String palavra;

    private String[] dicas;

    public PalavraSecreta(String palavra, String[] dicas) {
        this.palavra = palavra;
        this.dicas = dicas;
    }


    public String getPalavra() {
        return palavra;
    }

    public String[] getDicas() {
        return dicas;
    }

}
